/*
 * MIT License
 *
 * Copyright (c) 2020 VarTmp7
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

/*
clean test jacocoTestReport sonarqube
commando per avviare il container di sonar qube
    docker run -d --name sonarqube -p 9000:9000 sonarqube
 Comando da usare per far andare sonar qube
 gradlew sonarqube -Dsonar.projectKey=Stalker_App -Dsonar.host.url=http://localhost:9000 -Dsonar.login=11ca1a408698561b7b343325b831cb3ad4b279bf


gradlew clean test createDebugCoverageReport jacocoTestDebugUnitTestReport sonarqube

*/
apply plugin: 'com.onesignal.androidsdk.onesignal-gradle-plugin'
apply plugin: 'com.android.application'
apply plugin: 'com.google.gms.google-services'
apply plugin: "androidx.navigation.safeargs"

buildscript {
    repositories {
        maven { url 'https://plugins.gradle.org/m2/'}
    }
    dependencies {
        classpath 'gradle.plugin.com.onesignal:onesignal-gradle-plugin:0.12.6'
        classpath "org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:2.8"

    }
}
apply plugin: 'org.sonarqube'
apply plugin: 'jacoco-android'


repositories {
    maven { url 'https://maven.google.com' }

}
android {
    def $sdkVersion=28
    compileSdkVersion $sdkVersion
    buildToolsVersion "28.0.3"
    defaultConfig {
        applicationId "com.vartmp7.stalker"
        minSdkVersion $sdkVersion
        targetSdkVersion $sdkVersion
        testApplicationId "com.vartmp7.stalkertest"
        testHandleProfiling true
        testFunctionalTest true
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "androidx.test.runner.AndroidJUnitRunner"
        manifestPlaceholders = [
                onesignal_app_id: '0549f894-64ee-40c5-8045-b00f6d70ed4f',
                // Project number pulled from dashboard, local value is ignored.
                onesignal_google_project_number: 'REMOTE'
        ]
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
        }
        debug {
            testCoverageEnabled  true
        }
    }

    testOptions {
        reportDir "$rootDir/test-reports"
        resultsDir "$rootDir/test-results"
        animationsDisabled true
        unitTests {
            returnDefaultValues true
            includeAndroidResources  =true
        }
        unitTests.all {
            systemProperty 'debug', 'true'

            jacoco {
                version '1.0.1'
            }
        }
    }
    compileOptions {
        sourceCompatibility = 1.8
        targetCompatibility = 1.8
    }
}

task createTestReport(type: JacocoReport, dependsOn: ['createDevDebugCoverageReport']) {
    group = "Reporting"

    reports {
        xml.enabled = true
        html.enabled = true
    }
/**
 * gradlew sonarqube non funziona per questo errore:
 * No files nor directories matching 'build/intermediates/classes/dev/debug'
 */
    def fileFilter = ['**/R.class',
                      '**/R$*.class',
                      '**/BuildConfig.*',
                      '**/*$ViewInjector*.*',
                      '**/*$ViewBinder*.*',
                      '**/*$MembersInjector*.*',
                      '**/Manifest*.*',
                      '**/*Test*.*',
                      'android/**/*.*']
    def debugTree = fileTree(dir: "${project.projectDir}/src/test/*/*Test.java", excludes: fileFilter)
    //def debugTree = fileTree(dir: "${buildDir}/intermediates/classes/dev/debug", excludes: fileFilter)
    def mainSrc = "${project.projectDir}/src/main/java"
    //def mainSrc = "${project.projectDir}/app/src/main/java"

    sourceDirectories.from = files([mainSrc])
    classDirectories.from = files([debugTree])
    executionData.from = files("${project.buildDir}/jacoco/testDevDebugUnitTest.exec")
    def files = fileTree("${buildDir}/outputs/code-coverage/connected/flavors/DEV/").filter { it.isFile() }.files.name
    def instrumentationFileName = "${buildDir}/outputs/code-coverage/connected/flavors/DEV/" + files[0];
}

sonarqube {
    properties {

        property "sonar.host.url", "http:localhost:9000"
        property "sonar.username", "admin"
        property "sonar.password", "admin"
        property "sonar.java.source","1.8"
        property "sonar.java.target","1.8"
        property "sonar.projectKey", "StalkerApp"
        property "sonar.projectName", "StalkerApp"
        property "sonar.projectVersion", "${version}"

        property "sonar.sources", "src/main"

        property "sonar.java.source", "8"
        property "sonar.java.binaries", "build/intermediates/app_classes"

        property "sonar.android.lint.report", "build/outputs/lint-results.xml"
        property "sonar.jacoco.xmlReportPaths","build/reports/jacoco/jacocoTestDebugUnitTestReport/jacocoTestDebugUnitTestReport.xml"
        property "sonar.jacoco.reportPath","build/jacocotestDebugUnitTest.exec"
        property "sonar.coverage.reportPath", "build/reports/jacoco/jacocoTestDebugUnitTestReport/jacocoTestDebugUnitTestReport.xml"
        property "sonar.jacoco.reportPath", "build/reports/jacoco/jacocoTestDebugUnitTestReport/jacocoTestDebugUnitTestReport.xml"
        property 'sonar.coverage.jacoco.xmlReportPaths','build/reports/jacoco/jacocoTestDebugUnitTestReport/jacocoTestDebugUnitTestReport.xml'

        def files = fileTree("${buildDir}/outputs/code-coverage/connected/flavors/DEV/").filter { it.isFile() }.files.name
        def instrumentationFileName = "${buildDir}/reports/coverage/debug/reports.xml"

        property "sonar.coverage.jacoco.itReportPath", instrumentationFileName
    }
}
dependencies {
    implementation fileTree(include: ['*.jar'], dir: 'libs')
    implementation 'androidx.appcompat:appcompat:1.1.0'
    implementation 'androidx.constraintlayout:constraintlayout:1.1.3'
    androidTestImplementation 'androidx.test.ext:junit:1.1.1'
    androidTestImplementation 'androidx.test:runner:1.2.0'
    androidTestImplementation 'androidx.test:rules:1.2.0'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'
    androidTestImplementation "org.mockito:mockito-android:2.23.4"
    androidTestImplementation "android.arch.core:core-testing:2.1.0"

    //per instrumented test dei fragment
    debugImplementation 'androidx.fragment:fragment-testing:1.2.4'


    testImplementation "org.mockito:mockito-core:2.23.4"
    androidTestImplementation 'androidx.arch.core:core-testing:2.1.0'
    testImplementation 'junit:junit:4.13'
    testImplementation 'com.squareup.okhttp3:mockwebserver:4.4.0'

    implementation files('libs/lombok.jar')
    implementation 'com.google.android.material:material:1.1.0'
    implementation 'com.google.firebase:firebase-analytics:17.3.0'
    implementation 'com.google.firebase:firebase-auth:19.3.0'
    implementation 'com.google.firebase:firebase-firestore:21.4.2'
    implementation 'com.google.code.gson:gson:2.8.6'
    implementation 'com.google.android.gms:play-services-auth:18.0.0'
    implementation 'com.google.android.material:material:1.1.0'
    implementation 'com.google.android.gms:play-services-location:17.0.0'
    implementation 'com.google.android.gms:play-services-base:17.2.1'

    implementation 'androidx.vectordrawable:vectordrawable:1.1.0'
    implementation 'androidx.navigation:navigation-fragment:2.2.1'
    implementation 'androidx.navigation:navigation-ui:2.2.1'
    implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'
    implementation 'androidx.cardview:cardview:1.0.0'
    implementation 'androidx.recyclerview:recyclerview:1.1.0'
    implementation 'androidx.swiperefreshlayout:swiperefreshlayout:1.0.0'
    implementation "androidx.preference:preference:1.1.0"

    implementation 'com.facebook.android:facebook-android-sdk:5.15.3'
    implementation 'com.squareup.okhttp3:okhttp:4.4.0'
    implementation 'com.onesignal:OneSignal:3.12.6'
    implementation 'com.unboundid:unboundid-ldapsdk:4.0.14'
    implementation 'de.hdodenhof:circleimageview:3.1.0'
    implementation 'com.github.bumptech.glide:glide:4.11.0'
    annotationProcessor 'com.github.bumptech.glide:compiler:4.11.0'
    implementation 'com.squareup.retrofit2:retrofit:2.8.1'
    implementation 'com.squareup.retrofit2:converter-gson:2.8.1'
    annotationProcessor 'org.projectlombok:lombok:1.18.12'
    annotationProcessor files("libs/lombok.jar")

    //DI
    implementation 'com.google.dagger:dagger:2.27'
    annotationProcessor 'com.google.dagger:dagger-compiler:2.27'


    // Java language implementation
    implementation 'com.cuneytayyildiz:onboarder:1.0.4'
}
